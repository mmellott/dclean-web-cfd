#!/usr/bin/python
""" Sushovan bins all of the year values when running BN learning alg.
Run this script on the dataset so by default our year data will be binned too.
"""
import sys

usage = "usage: bin.py csvf_tobin attr_i"
if len(sys.argv) != 3: sys.exit(usage)

# don't forget this is an index
attr_i = int(sys.argv[2])
csvf_name = sys.argv[1]

# open file, get lines, and close (we gonna write to it later)
csvf = open(csvf_name)
lines = csvf.readlines()
csvf.close()

# mod every line in lines with binned year
for i in range(0, len(lines)):
    parts = lines[i].strip().split(",") # hopefully to field is just a space...
    year = parts[attr_i]

    try:
        year = int(year)
        
        # range(a,b) -> [a,b) of ints, thus the +1
        if year in range(2011, 2013 + 1): year = "2011-13"
        elif year in range(2008, 2010 + 1): year = "2008-10"
        elif year in range(2005, 2007 + 1): year = "2005-07"
        elif year in range(2000, 2004 + 1): year = "2000-04"
        elif year in range(1990, 1999 + 1): year = "90s"
        elif year in range(1980, 1989 + 1): year = "80s"
        elif year < 1980: year = "<1980"
        else:
            # CAR MADE IN THE FUTURE...
            sys.exit("error: car made in the future? year: %i" % year)
        
    except: # year not an int, prob dirty
        year = "NA"
    
    parts[attr_i] = year
    lines[i] = ",".join(parts)

# write modded lines to file
csvf = open(csvf_name, "w")
for line in lines: csvf.write(line + "\n")
csvf.close()

