#!/bin/bash

function load {
    echo "loading cars2/$1.txt"
    ./load.py cars2/$1.txt cars2 $1
}

# please ignore the next two lines. nothing to see here.
mysql -u root -e 'drop database cars2;'
mysql -u root -e 'create database cars2;' 

# load database cars2 with following tables
load 00p
load 01p
load 02p
load 03p
load 04p
load 05p
load 10p
load 15p
load 20p
load 25p
load 30p
load 35p

