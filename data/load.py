#!/usr/bin/python
################################################################################
# This script was just used to load the text file db's Sushovan gave to me.
#
################################################################################
import mysql.connector
import sys

if len(sys.argv) != 4: sys.exit("usage: load.py fname db_name tname")

fname = sys.argv[1]
db_name = sys.argv[2]
tname = sys.argv[3]

config = {
  'user': 'root',
  'host': '127.0.0.1',
  'database': db_name,
  'raise_on_warnings': True
}

create_table = """create table %s
    (model TEXT,
    make TEXT,
    type TEXT,
    year TEXT,
    cond TEXT,
    drivetrain TEXT,
    doors TEXT,
    engine TEXT);
""" % (tname)

insert_cmd = """insert into %s
    value("%s","%s","%s","%s","%s","%s","%s","%s")
"""

cnx = None
try:
    # conn to my db
    cnx = mysql.connector.connect(**config)
    cursor = cnx.cursor()
   
    # start doing some sql stuff
    cursor.execute(create_table)

    for line in open(fname):
        parts = line.strip().split(",")
        cmd = insert_cmd % (tname, parts[0],parts[1],parts[2],parts[3],parts[4],
          parts[5],parts[6],parts[7])
        cursor.execute(cmd)
        
except mysql.connector.Error as err:
    print err 

if cnx: cnx.close()

