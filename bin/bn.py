#!/usr/bin/pyhton
"""
This file only contains the BayesNet class. This class can be used to represent
an arbitrary Bayes Net.
"""

class BayesNet:
    """ This class represents a bayes net. """

    def __init__(self, lookup_s2i):
        """ The one and only ctor. """
        self._cpts = {} # keys are tuples of attr indexes and order matters
        self._given_attrs_ts_i = {} # given attr index tuples (index by attr)
        self._lookup_s2i = lookup_s2i 


    def add_cpt(self, given_attrs_t_s, cpt_d):
        """ adds a cpt associated with the given attribute """

        # given_attrs_t_s format:
        # (a,b,c, ... ,z)  :  (a & b & c & ... -> z)
        attr_s = given_attrs_t_s[-1] # attr_s is implied by rest of tuple

        # get attr's index and then add cpt to cpts
        attr_i = self._lookup_s2i[attr_s]
        self._cpts[attr_i] = cpt_d 
        
        
        # given_t has string fields, convert and store
        given_attrs_t_i = ()
        for a_s in given_attrs_t_s:
            given_attrs_t_i += (self._lookup_s2i[a_s],)

        self._given_attrs_ts_i[attr_i] = given_attrs_t_i 

    # TODO: prob and _a_prob should be pushed somewhere else. These methods are
    # really project specific and not fundamental Bayes Net operations.
    def prob(self, parts):
        """ computes the prob of parts by evaluating the joing prob of each
        attr given evidence. parts can be a tuple or a list """
        joint_p = 1

        for i in range(0, len(parts)):
            joint_p *= self._a_prob(i, parts)

        return joint_p 


    def _a_prob(self, attr_i, parts):
        """ computes the prob of an attr given evidence contained within
        a tuple that has been split into parts """
        given_t_i = self._given_attrs_ts_i[attr_i]
        cpt = self._cpts[attr_i] # this should not throw KeyError

        # build the key tuple
        key_t = ()
        for i in range(0, len(given_t_i)):
            key_t = key_t + (parts[given_t_i[i]],)


        # this might throw key error, that's fine
        # this is because Sushovan used a reduced db when learning bn
        try: return cpt[key_t]
        except KeyError: return 0.0
        
