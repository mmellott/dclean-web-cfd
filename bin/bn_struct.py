#!/usr/bin/python
"""
Get Bayes Net structure from BN output filesystem.
"""
import os
import sys

def get_sides(fname_l):
    """ Returns (lhs, rhs) structural implications. """
    lhs = []
    rhs = []

    for fname in fname_l:
        # check if file reps bn structure
        if fname[0:3] == "bn-":
            # bild parts, strip file extension is split on -
            parts = fname.strip().split(".")
            parts = parts[0].split("-")

            # build rhs
            rhs.append(parts[1])

            # build lhs, when file does not have a given: parts[1] --> parts[1]
            if len(parts) == 4:
                parts = parts[3].split(",")
                for i in range(0, len(parts)):
                    lhs.append(parts[i])

            else: lhs.append(parts[1])

    return (lhs, rhs)
            

if __name__ == "__main__":
    if len(sys.argv) != 2: sys.exit("usage: bn_struct.py bn-dir/")

    fname_l = os.listdir(sys.argv[1])
    (lhs, rhs) = get_sides(fname_l)

    print lhs
    print rhs

