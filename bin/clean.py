#!/usr/bin/python
"""
Clean a MySQL table based on .cfd file.
"""
import sys
import json

import ftable
import cfd


def table_diff(dirty_tl_before, dirty_tl_after, clean_tl):
    """ Print stats on a drity table before and after cleaning. """
    # build stats
    d2c = 0
    c2d = 0
    num_d = 0
    num_tups = len(clean_tl)

    for i in range(0, num_tups):
        if dirty_tl_before[i] != clean_tl[i]: num_d += 1

        if dirty_tl_after[i] != dirty_tl_before[i]:
            if clean_tl[i] == dirty_tl_after[i]:
                d2c += 1
            else: c2d += 1

    print ("total (tuples, num_d, d2c, c2d) : (%i, %i, %i, %i)"
      % (num_tups, num_d, c2d, d2c))
    #print ("tuples correctly cleaned: %i (%f%% of whole)"
    #  % (d2c, 100*float(d2c)/num_tups))
    #print ("tuples cleaned incorrectly: %i (%f%% of whole)"
    #  % (c2d, 100*float(c2d)/num_tups))
    print ("net cleaning: %i (%f%% of dirty)"
      % (d2c - c2d, 100*float(d2c - c2d)/num_d))

 
if __name__ == "__main__":
    usage = "usage: clean.py dirty_csv_fname clean_csv_fname cfd_fname schema.json [bn-dir/]"
    if len(sys.argv) < 5 or len(sys.argv) > 6:
        sys.exit(usage)

    # get cmd line args
    dirty_csv_fname = sys.argv[1]
    clean_csv_fname = sys.argv[2]
    cfd_fname = sys.argv[3]
    schema_fname = sys.argv[4]
    bn_dir = ""
    if len(sys.argv) == 6:
        bn_dir = sys.argv[5]

    # build attrs_s_l and atrrs_i2s ... hehe
    attr_s_l = attrs_i2s = json.load(open(schema_fname))["attrs"]

    # init tables, get dirty table before cleaning, and get clean table 
    # don't know about mult conns, so close clean table b4 opening dirty
    clean_table = ftable.FastTable(attr_s_l, clean_csv_fname)
    clean_tl = clean_table.get_tuples()
    clean_table.close()

    dirty_table = ftable.FastTable(attr_s_l, dirty_csv_fname)
    dirty_tl_before = dirty_table.get_tuples()

    # get CFDs in set/where dict form
    (set_d_l, where_d_l) = ({}, {})
    if bn_dir == "":
        (set_d_l, where_d_l) = cfd.files2unfiltered_cfds(cfd_fname, attrs_i2s)
    else:
        (set_d_l, where_d_l) = cfd.files2filtered_cfds(cfd_fname, attrs_i2s,
          bn_dir)

    # clean and print num affected for each CFD
    tmp_d_tl_before = dirty_tl_before
    for i in range(0, len(set_d_l)):
        num = dirty_table.update(set_d_l[i], where_d_l[i])
        print str(num) + ": " + str(where_d_l[i]) + " --> " + str(set_d_l[i])
        
        tmp_d_tl_after = dirty_table.get_tuples()
        table_diff(tmp_d_tl_before, tmp_d_tl_after, clean_tl)
        tmp_d_tl_before = tmp_d_tl_after

        print ""

    print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    
    # get table after cleaning and compare
    dirty_tl_after = dirty_table.get_tuples()
    table_diff(dirty_tl_before, dirty_tl_after, clean_tl)

