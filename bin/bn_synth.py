#!/usr/bin/python
"""
Compute pvals based on bn input. A lot of this is hardcoded for now.

Assumes this dir structure:
dir/
dir/bn-cartype-given-drivetrain,doors.txt
dir/bn-condition.txt
dir/bn-doors.txt
dir/bn-drivetrain-given-model.txt
dir/bn-engine-given-drivetrain.txt
dir/bn_learning.txt
dir/bn-make.txt
dir/bn-model-given-make,condition.txt
dir/bn-year-given-condition.txt
dir/fulldb.txt
dir/ground_truth.txt
"""
import bn

class BNSynthCarsDB(bn.BayesNet):
    """ implementation specific Bayes Net fn's for Sushovan's synth car DB """

    # TODO: make this into static method
    def bn_file2dict(self, fname):
        """ reads in a bn file and returns a dict """
        bn_d = {}
        for line in open(fname):
            parts = line.strip().split("\t")
            key = tuple(parts[0:-1]) # key includes A s.t. P(A|B) = parts[-1]
            bn_d[key] = float(parts[-1]) # parts[-1] = P(A|B)
        return bn_d


    def __init__(self, bn_dir):
        """ the one and only ctor """

        # these attrs are of course specific to the synth car DB
        attrs = { "model":0, "make":1, "cartype":2, "year":3, "cond":4,
            "drivetrain":5,
            "doors":6, "engine":7 }

        # ini the bayes net
        bn.BayesNet.__init__(self, attrs)

        # ini cpt's based on dir struct outlined above
        self.add_cpt(("drivetrain", "doors", "cartype"),
          self.bn_file2dict(bn_dir + "bn-cartype-given-drivetrain,doors.txt"))

        self.add_cpt(("cond",),
          self.bn_file2dict(bn_dir + "bn-condition.txt"))

        self.add_cpt(("doors",),
          self.bn_file2dict(bn_dir + "bn-doors.txt"))

        self.add_cpt(("model", "drivetrain"),
          self.bn_file2dict(bn_dir + "bn-drivetrain-given-model.txt"))

        self.add_cpt(("drivetrain", "engine"),
          self.bn_file2dict(bn_dir + "bn-engine-given-drivetrain.txt"))

        self.add_cpt(("make",),
          self.bn_file2dict(bn_dir + "bn-make.txt"))

        self.add_cpt(("make", "cond", "model"),
          self.bn_file2dict(bn_dir + "bn-model-given-make,condition.txt"))

        self.add_cpt(("cond", "year"),
          self.bn_file2dict(bn_dir + "bn-year-given-condition.txt"))

    def prob_tuple_l(self, tuple_l):
        """ computes joint prob for every tup in tuple_l """
        joint_ps = []
        for tup in tuple_l:
            joint_ps.append(self.prob(tup))
        return joint_ps

