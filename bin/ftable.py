#!/usr/bin/python
"""
Faster table implementation that can replace my table.MySQLTable class.

Speed gains are come from the fact that everything is stored in memory and
we are bypassing the TCP/IP stack. Orginally used the MySQLTable class
becuase I didn't have to implement things like an update command, but it
is pretty slow. The class contained within this file will probably not work
well for really big databases and so MySQLTable should be used in that case.
"""
import sys
import copy

class FastTable:
    """ Data table that can be loaded from csv file. Should be faster than
    a MySQLTable when all data can fit in memory. """

    def __init__(self, attr_s_l, csv_fname=None):
        """ Construct a FastTable with optional loading. """
        self._attr_s_l = attr_s_l # a list of the string values for attrs

        # construct lookup attr str to int lookup table
        self._attr_s2i = {}
        for i in range(0, len(attr_s_l)):
            self._attr_s2i[attr_s_l[i]] = i
        
        # load data or ini with empty list
        if csv_fname: self.load(csv_fname)
        else: self._data = []


    # TODO: this needs to be a static method
    def check_csvf_format(self, csv_fname):
        """ Returns true if file meets Sushovan's CSV file format specs. """
        num_cols = len(self._attr_s_l)

        # check every tuple has correct number of fields
        for line in open(csv_fname):
            if num_cols != len(line.strip().split(',')):
                return False

        return True


    def load(self, csv_fname):
        """ Load data from file with Sushovan's CSV format. """ 
        self._data = []

        # TODO: use the actual CSV file class ?
        for line in open(csv_fname):
            tup = tuple(line.strip().split(','))
            self._data.append(tup)


    def get_tuples(self):
        """ Return a deep copy of the list of tuples this table contains. """
        return copy.deepcopy(self._data)


    def get_top_50p(self, pvals):
        """ Get the top 50% of tuples with the highest pval. """
        zipper = zip(self._data, pvals)
        zipper = sorted(zipper, key=(lambda x: x[1]), reverse=True)

        # build top_50p list
        num_50p = len(self._data)/2
        del zipper[num_50p:]

        for i in range(0, len(zipper)):
            zipper[i] = zipper[i][0]

        return zipper


    def get_attr_list(self):
        """ Return a deep of the attribute list. """ 
        return copy.deepcopy(self._attr_s_l)


    def close(self):
        """ Does nothing...just here for MySQLTable compatability. """
        pass


    def update_l(self, set_d_l, where_d_l):
        """ Update data based on dict lists. """
        for i in range(0, len(set_d_l)):
            self.update(set_d_l[i], where_d_l[i])


    def update(self, set_d, where_d):
        """ Update data based on dicts and return number affected. """
        num_affected = 0
        for tup_i in range(0, len(self._data)):
            if self._where_match(tup_i, where_d):
                self._set(tup_i, set_d)
                num_affected += 1
        return num_affected


    def _set(self, tup_i, set_d):
        """ Set tup based upon set_d. """
        set_keys = set_d.keys()
        tup_as_l = list(self._data[tup_i])

        for key in set_keys:
            tup_as_l[self._attr_s2i[key]] = set_d[key]

        self._data[tup_i] = tuple(tup_as_l)


    def _where_match(self, tup_i, where_d):
        """ Check to see if tup matches the where_d. """ 
        where_keys = where_d.keys()
        tup = self._data[tup_i]

        for key in where_keys:
            if tup[self._attr_s2i[key]] != where_d[key]:
                return False

        return True


if __name__ == "__main__":
    if len(sys.argv) != 2: sys.exit("usage: ftable.py csv_fname")

    attr_s_l = [ "model", "make", "type", "year", "cond", "drivetrain",
      "doors", "engine" ]
    csv_fname = sys.argv[1]

    mytable = FastTable(attr_s_l)
    if mytable.check_csvf_format(csv_fname):
        mytable.load(csv_fname)
    else: print "Invalid file?"

    pass

