#!/usr/bin/python
"""
Main driver for pval part of project.
"""
# std modules
import sys
import json

# my modules
import ftable
import bn_synth


if __name__ == "__main__":
    usage = "usage: pval.py bn_dir csv_fname schema.json out_fname"
    if len(sys.argv) != 5: sys.exit(usage);

    # parse cmd line args
    bn_dir = sys.argv[1] 
    csv_fname = sys.argv[2]
    schema_fname = sys.argv[3]
    outf_name = sys.argv[4]

    # get some objs
    attr_s_l = json.load(open(schema_fname))["attrs"]
    mytable = ftable.FastTable(attr_s_l, csv_fname)
    bn_i = bn_synth.BNSynthCarsDB(bn_dir)
 
    # get the stuff we'll need
    tuple_l = mytable.get_tuples()
    attrs = mytable.get_attr_list()

    # get pvals sort
    pvals = bn_i.prob_tuple_l(tuple_l) 
    tuple_l_top = mytable.get_top_50p(pvals)

    # write top tups to file
    outf = open(outf_name, "w")
    for tup in tuple_l_top: outf.write(",".join(tup) + '\n')
    outf.close()

