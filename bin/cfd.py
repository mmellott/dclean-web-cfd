#!/usr/bin/python
"""
Parse graph.pl CFD output file and clean table.
"""
import sys
import os
import json

import bn_struct


def get_cfd_lines(lines):
    """ Examines every line in lines and returns a list of the ones that
    represent CFDs. """
    start_i = 0
    end_i = 0
    
    # find start/end index of cfd region
    # using not in operator makes it a little more robust
    while "DISCOVERED CFDs:" not in lines[start_i]:
        start_i += 1

    while "Unsatisfying Reps" not in lines[end_i]:
        end_i += 1

    # get lines that rep cfds in cfd region
    cfd_lines = []
    for i in range(start_i, end_i):
        line = lines[i] # oh python...why do you make me obsess over readibility
        if "condition on" in line:
            cfd_lines.append(line)
    
    return cfd_lines


def cfd_lines2dict_ls(cfd_lines, attrs_i2s):
    """ Parses lines of text that represent CFDs and returns
    (set_d_l, where_d_l). """
    where_d_l = []
    set_d_l = []

    for line in cfd_lines:
        # det if line contains useful cfd
        if len(line.split("-->")) == 3: 
            parts = line.strip().split(": ") # strip for good measure

            # lhs and rhs of left hand implication
            lhi_parts = parts[0].split(" ")[1].split("-->")
            lhs_lhi_parts = lhi_parts[0].split('-') # len=2 because useful cfd
            rhs_lhi_parts = lhi_parts[1].split('-')

            # lhs and rhs of right hand imp
            rhi_parts = parts[2].split("-->")
            lhs_rhi_parts = rhi_parts[0].split(':') # rhi use diff seperator
            rhs_rhi_parts = rhi_parts[1].split(':')

            # get condition on
            cond_attrs_si = parts[1].split(" ")[2].split('-')

            # build the where dict
            where_d = {}
            for i in range(0, len(cond_attrs_si)):
                attr_s = attrs_i2s[int(cond_attrs_si[i])] 
                where_d[attr_s] = lhs_rhi_parts[i]

            # build the set dict
            set_d = {}
           
            filter_me = [] 
            for i in range(0, len(rhs_lhi_parts)):
                for j in range(0, len(lhs_lhi_parts)):
                    if lhs_lhi_parts[j] == rhs_lhi_parts[i]:
                        filter_me.append(rhs_lhi_parts[i])

            for item in filter_me:
                rhs_lhi_parts.remove(item)

            set_d = {}
            for i in range(0, len(rhs_lhi_parts)):
                attr_s = attrs_i2s[int(rhs_lhi_parts[i])] 
                set_d[attr_s] = rhs_rhi_parts[i]

            where_d_l.append(where_d)
            set_d_l.append(set_d)

    return (set_d_l, where_d_l)


def build_mysql_cmds(set_d_l, where_d_l, tname):
    """ Builds a list of MySQL update commands for table tname from the input
    dict lists. """
    cmds = []

    for i in range(0, len(set_d_l)):
        # get dicts for this cmd
        set_d = set_d_l[i]
        where_d = where_d_l[i]

        # base cmd
        cmd = "update %s" % (tname) 

        # add set parts
        cmd += " set"
        for key in set_d.keys():
            cmd += " " + key + "='" + set_d[key] + "', " 
        cmd = cmd.rstrip(", ")

        # add where parts
        cmd += " where"
        for key in where_d.keys():
            cmd += " " + key + "='" + where_d[key] + "' and" 
        cmd = cmd.rstrip(" and")

        # fin off cmd with a ; and put into cmds
        cmd += ";"
        cmds.append(cmd)

    return cmds


def get_lines(cfd_fname):
    """ Read in lines from cfd file. """
    return [line.strip() for line in open(cfd_fname)]


def filter_dict_ls(set_d_l, where_d_l, lhs, rhs):
    """ Returns new (set_d_l, where_d_l) filtered bashed on (lhs, rhs). """
    new_set_d_l = []
    new_where_d_l = []

    for i in range(0, len(set_d_l)):
        # get dict/list we are going to analyze
        where_d = where_d_l[i]

        # if where d has a key that is not in lhs
        we_need_to_del = False
        for key in where_d.keys():
            if key not in lhs:
                we_need_to_del = True
                break

        if not(we_need_to_del):
            new_set_d_l.append( set_d_l[i] )
            new_where_d_l.append( where_d_l[i] )

    return (new_set_d_l, new_where_d_l)


def cfd_file2mysql_cmds(cfd_fname, tname, attrs_i2s):
    """ Get the MySQL update commands for tname that correspond to the CFDs
    contained within the file cfd_fname. """ 
    lines = get_lines(cfd_fname)
    cfd_lines = get_cfd_lines(lines)
    (set_d_l, where_d_l) = cfd_lines2dict_ls(cfd_lines, attrs_i2s)
    cmds = build_mysql_cmds(set_d_l, where_d_l, tname)
    return cmds


def files2filtered_cfds(cfd_fname, attrs_i2s, bn_dir):
    """ Get a (set_d_l, where_d_l) based upon the file named cfd_fname and
    filter based upon (lhs, rhs). """
    (lhs, rhs) = bn_struct.get_sides(os.listdir(bn_dir))
    lines = get_lines(cfd_fname)
    cfd_lines = get_cfd_lines(lines)
    (set_d_l, where_d_l) = cfd_lines2dict_ls(cfd_lines, attrs_i2s)
    (set_d_l, where_d_l) = filter_dict_ls(set_d_l, where_d_l, lhs, rhs)
    return (set_d_l, where_d_l)


def files2unfiltered_cfds(cfd_fname, attrs_i2s):
    """ Get a (set_d_l, where_d_l) based upon the file named cfd_fname.
    No filters will be applied. """
    lines = get_lines(cfd_fname)
    cfd_lines = get_cfd_lines(lines)
    (set_d_l, where_d_l) = cfd_lines2dict_ls(cfd_lines, attrs_i2s)
    return (set_d_l, where_d_l)

