#!/bin/bash
# Run CFD learning program on cars2 data. This could be considered the "naive"
# cleaning method.

function run {
    cd cfd
    ./run.sh ../data/cars2/$1.txt ../out/naive/$1.cfd
    cd ..
}

run 01p
run 02p
run 03p
run 04p
run 05p
run 10p
run 15p
run 20p
run 25p
run 30p
run 35p

