#!/usr/bin/python
"""
Use this script to run CFD learning code on input db.

This file was written to automate the CFD learning
process for multiple input DBs. Everything banks on
the hardcoded test.* lines the various files that are
involved in the process.

Input DB should be in csv format.
  TODO: validate csv format?
"""
from subprocess import call
import sys

if len(sys.argv) != 3: sys.exit("usage: learn.py in_db.txt out.log")

in_fname = sys.argv[1]
out_fname = sys.argv[2]

print "running graph.pl on %s" % in_fname

call(["cp", in_fname, "test.orig"])
call(["./select.perl", "test.dsc"])
call(["./graph.pl", ">", out_fname]) 
call(["rm", "test.orig", "test.dat", "test.rel", "test.atr"])

