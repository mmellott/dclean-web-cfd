#!/bin/bash
# This file was written to automate the CFD learning
# process for multiple input DBs. Everything banks on
# the hardcoded test.* lines the various files that are
# involved in the process.

function run {
    echo "running graph.pl on $1"
    cp $1 test.orig
    ./select.perl test.dsc
    ./graph.pl > $2
    rm test.orig test.dat test.rel test.atr
}

EXPECTED_ARGS=2
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
  echo "Usage: `basename $0` in_db.txt out.log"
  exit $E_BADARGS
fi

run $1 $2

