#!/bin/bash
# Run clean.py on all of our tables.

function run {
    echo "running cleaner on $1"
    dir=data/cars2
    out_p=out
    args="$dir/$1.txt $dir/00p.txt $out_p/$1.cfd $dir/schema.json $dir/bn-$1/"
    bin/clean.py $args > $out_p/perfs/$1.log
}

# get some work done
run 01p
run 02p
run 03p
run 04p
run 05p
run 10p
run 15p
run 20p
run 25p
run 30p
run 35p

