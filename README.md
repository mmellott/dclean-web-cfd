An Attempt at Data Cleaning
============================

Project "Abstract" 
-------------------
The problem here is data cleaning. We want to clean a dirty database but do not
have a large/clean training sample. Most data cleaning approaches rely on such
a sample and learn CFDs, but I am developing a system that can learn CFDs from
the dirty database itself. The method can be summarized as follows:

1. learn Bayes Net
2. filter DB by computing confidence values based upon Bayes Net
3. learn CFDs from filtered database
4. clean actual database with CFDs

Status
-------
Need to clean using "real" CFDs. My cleaner was only using "degenerate" CFDs
(basically just association rules). This could make the method much more
effective.

The changes outlined above will require some MAJOR updates...

