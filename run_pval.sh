#!/bin/bash
# Runs pval.py on all of our cars2 data and then runs CFD learning program on
# resulting filtered data.

function run {
    echo "running pval on $1"

    data_d=data/cars2 
    bin/pval.py $data_d/bn-$1/ $data_d/$1.txt $data_d/schema.json tmp.txt

    if [ -e tmp.txt ]
    then
        cd cfd
        ./run.sh ../tmp.txt ../out/$1.cfd
        cd ..
        rm tmp.txt
    else
        echo "tmp.txt does not exist, check pval.py"
    fi

    echo ""
}

run 01p
run 02p
run 03p
run 04p
run 05p
run 10p
run 15p
run 20p
run 25p
run 30p
run 35p

